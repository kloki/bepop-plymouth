instructions

copy to correct folder
sudo cp bebop/ /usr/share/plymouth/themes/ -r

update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth /usr/share/plymouth/themes/bebop/bebop.plymouth 100

sudo update-alternatives --config default.plymouth
sudo update-initramfs -u